/** I still love you */
package proyecto_cajero_automatico;

import java.util.Scanner;

public class Proyecto_Cajero_Automatico {

    static Scanner sc = new Scanner(System.in);
    static Usuarios usuarioLogueado;
    static Usuarios[] usuariosDelBanco = new Usuarios[3];
    static double tiempoCarga;

    static void im(String sMensaje) {
        System.out.println(sMensaje);
    }

    static void separador() {
        im("-----------------------------------------");
    }

    static void encabezado() {
        im("Universidad Autónoma de Campeche");
        im("Facultad de Ingeniería");
        im("Ingeniería en Sistemas Computacionales");
        im("Lenguaje de Programación 2 - A");
        im("Juan Pablo Diaz León - 63832");
        im("Juan Carlos Pinzón Medina - 51855");
        im("Emiliano Ayala Canúl - 55145");
        im("Carlos Armando Xool Medina - 63833");
        im("José Jesús Contreras Ibañez - 49510");
        separador();
    }

    static void piepagina() {
        separador();
        im("(c) UACAM-FI-ISC-LP2-A");
        im("Programa desarrollado por el mejor equipo de sistemas.");
        im("Derechos reservados, versión 1.28");
    }

    static void crearInfoUsuarios() {
        Usuarios Usuario1 = new Usuarios();
        Usuario1.nombre = "Carolina";
        Usuario1.pasword = "123";
        Usuario1.saldo = 1000;
        Usuario1.tipoCliente = "Cliente frecuente";

        //imprimirMensaje(Usuario1.toString());
        Usuarios Usuario2 = new Usuarios();
        Usuario2.nombre = "Perla";
        Usuario2.pasword = "321";
        Usuario2.saldo = 1500;
        Usuario2.tipoCliente = "Cliente plus";
        //imprimirMensaje(Usuario2.toString());

        Usuarios Usuario3 = new Usuarios();
        Usuario3.nombre = "Maria";
        Usuario3.pasword = "abc";
        Usuario3.saldo = 500;
        Usuario3.tipoCliente = "Cliente ocasional";
        //imprimirMensaje(Usuario3.toString());
        usuariosDelBanco[0] = Usuario1;
        usuariosDelBanco[1] = Usuario2;
        usuariosDelBanco[2] = Usuario3;

    }

    /**
     * Detiene la ejecucion del programa para simular un tiempo de carga
     *
     * @param msg un mensaje para desplegar mientras se espera
     * @param tiempoMilisegundos tiempo a detener en milisegundos
     * @version 1.0
     */
    static void dormir(String msg, long tiempoMilisegundos) {
        System.out.println(msg);
        try {
            Thread.sleep(tiempoMilisegundos);
        } catch (InterruptedException x) {

        }
    }

    /**
     * Realiza el proceso de inicio de sesion y comprueba los datos ingresados
     * con los datos de clientes del banco existentes
     *
     * @version 1.1
     */
    static void login() {
        String nomb;
        String pass;

        System.out.println("Bienvenido/a al banco de sistemas");
        System.out.println("Ingrese su nombre de usuario");
        nomb = sc.nextLine();
        System.out.println("Ingrese su contraseña");
        pass = sc.nextLine();
        for (Usuarios usuarioYaRegistrado : usuariosDelBanco) {
            if (nomb.equals(usuarioYaRegistrado.nombre)) {
                if (pass.equals(usuarioYaRegistrado.pasword)) {
                    dormir("Iniciando sesion...", 1000);
                    usuarioLogueado = usuarioYaRegistrado;
                }
            }
        }
    }

    /**
     * Muestra el menu principal, no termina hasta que la respuesta este dentro
     * del rango de opciones
     *
     * @return resp el numero que elija el usuario
     * @version 1.0
     */
    static int getRespuestaMenuInicial() {
        int resp;
        //imprimirMensaje("Bienvenido/a " + usuarioLogueado.nombre + " al Cajero automatico del mejor equipo de sistemas");
        separador();
        im("Bienvenido/a " + usuarioLogueado.tipoCliente + " " + usuarioLogueado.nombre + " al Cajero automático del mejor equipo de sistemas");
        im("Cuenta con un saldo de $" + usuarioLogueado.saldo + " pesos");
        im("Ingrese el codigo de la operacion que desesa realizar");
        im("1: Compra de tiempo aire");
        im("2: Pago de la luz");
        im("3: Pago del agua");
        im("4: Retiro de efectivo");
        im("5: Salir");
        im("Ingrese el número del servicio deseado: ");
        resp = sc.nextInt();

        //Mientras se ingrese un numero fuera del rango 1-5 seguira pidiendo un numero
        if (resp <= 0 || resp >= 6) {
            resp = getRespuestaMenuInicial();
        }

        return resp;
    }

    /**
     * Muestra el mensaje de cancelacion de un proceso y regresa al menu
     *
     * @param proceso el proceso que se cancelo, retiro, pago de x servicio, etc
     * @param razon la razon por la que el proceso se cancelo
     * @version 1.1
     */
    static void cancelarProceso(String proceso, String razon) {
        System.out.println("El proceso de '" + proceso + "' ha sido cancelado debido a '" + razon + "'");
        System.out.println("Presione Enter para regresar al menu principal");
        sc.nextLine();
        sc.nextLine();
        separador();
        //desarrolloXD(usuarioLogueado);
    }

    /**
     * Comprueba que el usuario tenga el saldo suficiente, si es asi se lo
     * descuenta
     *
     * @param desc cuanto dinero se le va a descontar al usuario
     * @version 1.1
     */
    static void descontarSaldoAlUsuario(int desc) {

        int nuevoSaldo = usuarioLogueado.saldo - desc;

        dormir("Procesando pago...", 2000);

        if (nuevoSaldo >= 0) {
            usuarioLogueado.saldo = nuevoSaldo;
            System.out.println("Se ha realizado un descuento de $" + desc + " pesos de su cuenta.");
            System.out.println("Saldo restante: $" + usuarioLogueado.saldo + " pesos");
        } else {
            System.out.println("No se a podido realizar el descuento de $" + desc + " pesos de su cuenta.");
            System.out.println("Su saldo no es suficiente para realizar la operacion");
        }
        System.out.println("Pulse Enter para volver al menu principal");
        separador();

        sc.nextLine();
        sc.nextLine();
    }

    @SuppressWarnings("InfiniteRecursion")
    static void desarrolloXD(Usuarios usuarioLogeado) {

        int costoAgua = 100;
        int respAgua;
        int costoLuz = 250;
        int respLuz = 0;
        int operARealizar = getRespuestaMenuInicial();
        int pagoRealizado = 0;
        int recargaDeseada;

        int retiro = 0;
        switch (operARealizar) {
            // Compra de tiempo aire

            case 1:
                separador();
                System.out.println("Ha elegido recargar tiempo aire, seleccione el plan deseado: ");
                System.out.println("1: Plan de 50");
                System.out.println("2: Plan de 100");
                System.out.println("3: Plan de 150");
                System.out.println("4: Cancelar selección de recargas");
                recargaDeseada = sc.nextInt();

                switch (recargaDeseada) {
                    case 1:
                        System.out.println("Realizando recarga de $50 pesos.");
                        pagoRealizado = 50;
                        break;
                    case 2:
                        System.out.println("Realizando recarga de $100 pesos");
                        pagoRealizado = 100;
                        break;
                    case 3:
                        System.out.println("Realizando recarga de $150 pesos");
                        pagoRealizado = 150;
                        break;
                    case 4:
                        cancelarProceso("recarga de tiempo aire", "cancelacion por el usuario");

                }

                if (pagoRealizado != 0) {
                    descontarSaldoAlUsuario(pagoRealizado);
                }
                break;

            // Pago de la luz
            case 2:
                System.out.println("Pagar Luz");
                System.out.println("El costo del pago de la luz es de $" + costoLuz + " pesos");
                System.out.println("¿Desea realizar el pago del servicio?");
                System.out.println("1: Realizar pago    2: Cancelar pago del servicio de luz");
                respLuz = sc.nextInt();
                switch (respLuz) {
                    case 1:
                        pagoRealizado = costoLuz;
                        descontarSaldoAlUsuario(pagoRealizado);
                        break;
                    case 2:
                        cancelarProceso("pago de luz", "cancelación por el usuario");
                        break;
                }
                break;

            //Pago del agua
            case 3:
                separador();
                System.out.println("Pago de agua");
                System.out.println("El costo del pago de agua es de $" + costoAgua + " pesos");
                System.out.println("Desea realizar el pago del servicio?");
                System.out.println("1: Realizar pago    2: Cancelar pago del servicio de agua");
                respAgua = sc.nextInt();
                switch (respAgua) {
                    case 1:
                        pagoRealizado = costoAgua;
                        descontarSaldoAlUsuario(pagoRealizado);
                        break;
                    case 2:
                        cancelarProceso("pago de agua", "cancelación por el usuario");
                        break;
                }

                break;

            //Retiro de efectivo
            case 4:
                //TODO Funcion para retirar efectivo
                separador();
                System.out.println("Ha elegido retirar efectivo, seleccione qué desea hacer: ");
                im("1: Retirar dinero de la cuenta");
                im("2: Cancelar servicio de retiro de efectivo");
                retiro = sc.nextInt();
                switch (retiro) {
                    case 1:
                        separador();
                        im("El saldo actual en su cuenta es de: " + usuarioLogueado.saldo + " pesos.");
                        im("El cajero solo cuenta con billetes de $50 pesos.");
                        im("Digite el monto a retirar: ");
                        retiro = sc.nextInt();
                        if (retiro % 50 == 0) {
                            if (retiro <= usuarioLogueado.saldo) {
                                pagoRealizado = retiro;
                                descontarSaldoAlUsuario(pagoRealizado);
                            }else{
                                cancelarProceso("Retiro de efectivo", "Saldo insuficiente");
                            }
                        }else{
                            cancelarProceso("Retiro de efectivo", "Solo se pueden retirar multiplos de 50");
                        }
                        break;
                    case 2:
                        cancelarProceso("retiro de efectivo", "cancelación por el usuario");
                        break;
                }
                break;
            //Salir
            case 5:
                //System.out.println("Cerrando sesion");
                dormir("Cerrando sesion...", 2000);
                return;
        }
        desarrolloXD(usuarioLogeado);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        crearInfoUsuarios();
        encabezado();

        ////////////////////////////////////////////
        ///*
        //Descomentar estas lineas cuando se empiece a trabajar 
        //con los usuarios que dejo el maestro
        login();
        try {
            desarrolloXD(usuarioLogueado);
        } catch (NullPointerException x) {
            System.out.println("Error al iniciar sesión, nombre de usuario o contraseña incorrectos");
            separador();
            //main(args);
        }
        //*/

        /*
        ////////////////////////////////////////////
        ////////////////////////////////////////////
        //Y comentar estas otras 
        usuarioLogueado = new Usuarios("Nombre de prueba", "contraseña", 250, "Cliente de prueba");
        desarrolloXD(usuarioLogueado);
        //
        ////////////////////////////////////////////
         */
        piepagina();

    }

}
