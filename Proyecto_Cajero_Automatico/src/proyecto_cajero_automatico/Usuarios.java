package proyecto_cajero_automatico;
public class Usuarios {
    
    String nombre;
    String    pasword;
    int    saldo;
    String tipoCliente;

    public Usuarios() {
    }

    public Usuarios( String nombre, String pasword, int saldo, String cliente) {
        this.nombre = nombre;
        this.pasword = pasword;
        this.saldo = saldo;
        this.tipoCliente = cliente;
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    @Override
    public String toString() {
        return "Usuario{" + ", nombre=" + nombre + ", pasword=" + pasword + ", saldo=" + saldo + ", cliente=" + tipoCliente + '}';
    }


}
